use crate::{
    block_forest::BlockForest,
    data::{BlockHash, TransactionHash, VerifiedBlock, VerifiedPeerMessage, VerifiedTransaction},
    node::mining_service::MiningInfo,
    node::peer_service::{PeerCommand, PeerCommandKind, PeerEvent, PeerEventKind, SessionId},
};

use anyhow::{Error, Result};
use crossbeam::{
    channel::{bounded, Receiver, Sender},
    select,
};
use log::*;
use rand::{seq::SliceRandom, thread_rng};
use serde::{Deserialize, Serialize};

use std::{
    collections::{HashMap, HashSet},
    sync::{Arc, Mutex},
    thread,
    time::Duration,
};

#[derive(Default, Serialize, Deserialize)]
pub struct GossipServiceConfig {
    #[serde(with = "humantime_serde")]
    pub eager_requests_interval: Duration,
}

pub struct GossipService {
    config: GossipServiceConfig,
    event_receiver: Receiver<PeerEvent>,
    command_sender: Sender<PeerCommand>,
    block_receiver: Receiver<VerifiedBlock>,
    mining_info_sender: Sender<MiningInfo>,
    block_forest: BlockForest,
    peers: HashMap<SessionId, PeerInfo>,
    mining_head: Arc<Mutex<Option<Arc<VerifiedBlock>>>>,
}

impl GossipService {
    pub fn new(
        config: GossipServiceConfig,
        event_receiver: Receiver<PeerEvent>,
        command_sender: Sender<PeerCommand>,
        block_receiver: Receiver<VerifiedBlock>,
        mining_info_sender: Sender<MiningInfo>,
    ) -> Self {
        GossipService {
            config,
            event_receiver,
            command_sender,
            block_receiver,
            mining_info_sender,
            block_forest: BlockForest::new(),
            peers: HashMap::new(),
            mining_head: Arc::new(Mutex::new(None)),
        }
    }

    pub fn run(&mut self) {
        let (eager_request_sender, eager_request_receiver) = bounded::<u8>(0);
        let interval = self.config.eager_requests_interval;
        if !interval.is_zero() {
            thread::spawn(move || loop {
                eager_request_sender.send(1).unwrap();
                thread::sleep(interval);
            });
        }
        loop {
            select! {
                recv(self.event_receiver) -> it => {
                    match it {
                        Ok(event) => {
                            self.handle_peer_event(event).map_err(|e| error!("{}", e)).unwrap();
                        }
                        Err(e) => error!("{}", e),
                    }
                }
                recv(self.block_receiver) -> it => {
                    match it {
                        Ok(block) => {
                            self.handle_block(block, None).map_err(|e| error!("{}", e)).unwrap();
                        },
                        Err(e) => error!("{}", e),
                    }
                }
                recv(eager_request_receiver) -> _ => {
                    self.request_unknown()
                }
            }
        }
    }

    fn handle_peer_event(&mut self, event: PeerEvent) -> Result<()> {
        match event.event_kind {
            PeerEventKind::Disconnected => {
                info!("Drop disconnected {} session", event.session_id);
                self.peers.remove(&event.session_id);
                self.command_sender
                    .send(PeerCommand {
                        session_id: event.session_id,
                        command_kind: PeerCommandKind::Drop,
                    })
                    .map_err(Error::from)
            }
            PeerEventKind::Connected => {
                info!(
                    "Sending head to connected {} session: {:?}",
                    event.session_id,
                    self.block_forest.head().index
                );
                self.command_sender.send(PeerCommand {
                    session_id: event.session_id,
                    command_kind: PeerCommandKind::SendMessage(VerifiedPeerMessage::Block(
                        Box::new(self.block_forest.head().as_ref().clone()),
                    )),
                })?;
                let mut peer_info = PeerInfo::default();
                self.block_forest
                    .pending_transactions()
                    .iter()
                    .filter(|(hash, _)| peer_info.transactions.insert(**hash))
                    .for_each(|(_, transaction)| {
                        info!(
                            "Sending pending transactions to connected {} peer",
                            event.session_id
                        );
                        self.command_sender
                            .send(PeerCommand {
                                session_id: event.session_id,
                                command_kind: PeerCommandKind::SendMessage(
                                    VerifiedPeerMessage::Transaction(Box::new(transaction.clone())),
                                ),
                            })
                            .map_err(|e| error!("{}: {}", event.session_id, e))
                            .ok();
                    });
                self.peers.insert(event.session_id, peer_info);
                Ok(())
            }
            PeerEventKind::NewMessage(message) => match message {
                VerifiedPeerMessage::Request { block_hash } => self
                    .block_forest
                    .find_block(&block_hash)
                    .map(|block| {
                        info!(
                            "Sending known block to {} peer: {:?}",
                            event.session_id, block.index
                        );
                        self.command_sender
                            .send(PeerCommand {
                                session_id: event.session_id,
                                command_kind: PeerCommandKind::SendMessage(
                                    VerifiedPeerMessage::Block(Box::new(block.as_ref().clone())),
                                ),
                            })
                            .map_err(Error::from)
                    })
                    .unwrap_or(Ok(())),
                VerifiedPeerMessage::Block(block) => {
                    info!("Adding block from {} peer", event.session_id);
                    self.handle_block(*block, Some(event.session_id))
                }
                VerifiedPeerMessage::Transaction(transaction) => {
                    info!("Adding transaction from {} peer", event.session_id);
                    self.handle_transaction(*transaction, &event.session_id)
                }
            },
        }
    }

    fn send_mining_info(&mut self) -> Result<()> {
        let head = self.block_forest.head();
        let mining_head = &mut *self.mining_head.lock().unwrap();
        if let Some(val) = mining_head {
            if val.index == head.index {
                return Ok(());
            }
        }
        debug!("Sending mining info");
        mining_head.replace(Arc::clone(head));
        let transactions = self
            .block_forest
            .pending_transactions()
            .iter()
            .map(|(_, t)| t.clone())
            .collect::<Vec<VerifiedTransaction>>();
        self.mining_info_sender
            .send(MiningInfo {
                block_index: head.index + 1,
                prev_hash: *head.hash(),
                max_hash: self.block_forest.next_max_hash(),
                transactions,
            })
            .map_err(Error::from)
    }

    fn request_unknown(&self) {
        let unknowns = self.block_forest.unknown_block_hashes();
        if !unknowns.is_empty() {
            let peer_vec = Vec::from_iter(self.peers.keys());
            if !peer_vec.is_empty() {
                let mut rng = thread_rng();
                self.block_forest
                    .unknown_block_hashes()
                    .iter()
                    .copied()
                    .for_each(|block_hash| {
                        let session_id = **peer_vec.choose(&mut rng).unwrap();
                        info!("Requesting unknown hash from {} peer", session_id);
                        self.command_sender
                            .send(PeerCommand {
                                session_id,
                                command_kind: PeerCommandKind::SendMessage(
                                    VerifiedPeerMessage::Request { block_hash },
                                ),
                            })
                            .unwrap();
                    })
            }
        }
    }

    fn handle_block(&mut self, block: VerifiedBlock, session_id: Option<SessionId>) -> Result<()> {
        let result = self
            .block_forest
            .add_block(block.clone())
            .map_err(|e| info!("Invalid block: {}", e));
        if result.is_ok() {
            session_id
                .map(|session_id| {
                    self.peers
                        .get_mut(&session_id)
                        .unwrap()
                        .blocks
                        .insert(*block.hash());
                    if self
                        .block_forest
                        .unknown_block_hashes()
                        .contains(&block.prev_hash)
                    {
                        info!("Requesting unknown hash from {} peer", session_id);
                        self.command_sender.send(PeerCommand {
                            session_id,
                            command_kind: PeerCommandKind::SendMessage(
                                VerifiedPeerMessage::Request {
                                    block_hash: block.prev_hash,
                                },
                            ),
                        })
                    } else {
                        Ok(())
                    }
                })
                .unwrap_or(Ok(()))?;
            self.send_mining_info()?;
            for (peer, info) in &mut self.peers {
                if !info.blocks.contains(block.hash()) {
                    info!("Sending new block to {} peer", peer);
                    self.command_sender.send(PeerCommand {
                        session_id: *peer,
                        command_kind: PeerCommandKind::SendMessage(VerifiedPeerMessage::Block(
                            Box::new(block.clone()),
                        )),
                    })?;
                    info.blocks.insert(*block.hash());
                }
            }
        }
        Ok(())
    }

    fn handle_transaction(
        &mut self,
        transaction: VerifiedTransaction,
        session_id: &SessionId,
    ) -> Result<()> {
        let result = self
            .block_forest
            .add_transaction(transaction.clone())
            .map_err(|e| info!("Invalid transaction: {}", e));
        Ok(result.is_ok())
            .map(|_| {
                self.peers
                    .get_mut(session_id)
                    .unwrap()
                    .transactions
                    .insert(*transaction.hash())
            })
            .and_then(|_| self.send_mining_info())
            .and_then(|_| {
                for (peer, info) in &mut self.peers {
                    if !info.transactions.contains(transaction.hash()) {
                        info!("Sending new transaction to {} peer", peer);
                        self.command_sender.send(PeerCommand {
                            session_id: *peer,
                            command_kind: PeerCommandKind::SendMessage(
                                VerifiedPeerMessage::Transaction(Box::new(transaction.clone())),
                            ),
                        })?;
                        info.transactions.insert(*transaction.hash());
                    }
                }
                Ok(())
            })
    }
}

#[derive(Default)]
struct PeerInfo {
    blocks: HashSet<BlockHash>,
    transactions: HashSet<TransactionHash>,
}
