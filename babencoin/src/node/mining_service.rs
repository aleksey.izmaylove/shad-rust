use std::{cmp::min, collections::VecDeque};

use crate::{
    data::{
        Block, BlockAttributes, BlockHash, Transaction, VerifiedBlock, VerifiedTransaction,
        WalletId,
    },
    util::{deserialize_wallet_id, serialize_wallet_id},
};

use anyhow::{Error, Result};
use chrono::Utc;
use crossbeam::channel::{Receiver, Sender};
use log::*;
use rayon::ThreadPoolBuilder;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct MiningServiceConfig {
    pub thread_count: usize,
    pub max_tx_per_block: usize,

    #[serde(
        serialize_with = "serialize_wallet_id",
        deserialize_with = "deserialize_wallet_id"
    )]
    pub public_key: WalletId,
}

impl Default for MiningServiceConfig {
    fn default() -> Self {
        Self {
            thread_count: 0,
            max_tx_per_block: 0,
            public_key: WalletId::of_genesis(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct MiningInfo {
    pub block_index: u64,
    pub prev_hash: BlockHash,
    pub max_hash: BlockHash,
    pub transactions: Vec<VerifiedTransaction>,
}

pub struct MiningService {
    config: MiningServiceConfig,
    info_receiver: Receiver<MiningInfo>,
    block_sender: Sender<VerifiedBlock>,
}

impl MiningService {
    pub fn new(
        config: MiningServiceConfig,
        info_receiver: Receiver<MiningInfo>,
        block_sender: Sender<VerifiedBlock>,
    ) -> Self {
        MiningService {
            config,
            info_receiver,
            block_sender,
        }
    }

    pub fn run(&mut self) {
        info!(
            "Creating mining pool with {} threads",
            self.config.thread_count
        );
        let thread_pool = ThreadPoolBuilder::new()
            .num_threads(self.config.thread_count)
            .build()
            .unwrap();
        let mut tx_queue = VecDeque::new();
        self.info_receiver.iter().for_each(|info| {
            if self.config.thread_count == 0 {
                return;
            }
            info.transactions
                .into_iter()
                .map(Transaction::from)
                .for_each(|it| tx_queue.push_back(it));
            let block_sender = self.block_sender.clone();
            let issuer = self.config.public_key.clone();
            let max_tx = self.config.max_tx_per_block;
            let transactions = tx_queue
                .drain(0..min(max_tx, tx_queue.len()))
                .collect::<Vec<Transaction>>();
            thread_pool.spawn(move || {
                Self::mine(
                    info.prev_hash,
                    info.max_hash,
                    info.block_index,
                    issuer,
                    transactions,
                    block_sender,
                )
                .map_err(|e| error!("Mining error: {}", e))
                .ok();
            })
        });
    }

    fn mine(
        prev_hash: BlockHash,
        max_hash: BlockHash,
        index: u64,
        issuer: WalletId,
        transactions: Vec<Transaction>,
        block_sender: Sender<VerifiedBlock>,
    ) -> Result<()> {
        info!("Start mining: {:?}", prev_hash);
        let mut block = Block {
            attrs: BlockAttributes {
                index,
                reward: 1,
                nonce: 0,
                timestamp: Utc::now(),
                issuer,
                max_hash,
                prev_hash,
            },
            transactions,
        };
        loop {
            if block.compute_hash() <= max_hash {
                break block.verified();
            }
            block.nonce += 1;
        }
        .and_then(|block| {
            info!("Have mined block: {:?}", block.hash());
            block_sender.send(block).map_err(Error::from)
        })
    }
}
