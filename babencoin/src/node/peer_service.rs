use crate::data::{PeerMessage, VerifiedPeerMessage};

use anyhow::{Error, Result};
use crossbeam::{
    channel::{bounded, Receiver, Sender},
    thread::scope,
};
use log::*;
use rand::random;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    io::{BufRead, BufReader, BufWriter, Read, Write},
    net::{Shutdown, TcpListener, TcpStream},
    sync::{Arc, Mutex},
    thread,
    time::Duration,
};

const BUF_SIZE: usize = ((2 << 10) * 64) + 1;
const BUF_SIZE_U64: u64 = BUF_SIZE as u64;
const READ_WRITE_TIMEOUT: Option<Duration> = Some(Duration::from_secs(60));
const SEPARATOR: [u8; 1] = [b'\0'];

pub type SessionId = u64;

#[derive(Default, Serialize, Deserialize)]
pub struct PeerServiceConfig {
    #[serde(with = "humantime_serde")]
    pub dial_cooldown: Duration,
    pub dial_addresses: Vec<String>,
    pub listen_address: Option<String>,
}

#[derive(Debug, Clone)]
pub struct PeerEvent {
    pub session_id: SessionId,
    pub event_kind: PeerEventKind,
}

#[derive(Debug, Clone)]
pub enum PeerEventKind {
    Connected,
    Disconnected,
    NewMessage(VerifiedPeerMessage),
}

#[derive(Debug, Clone)]
pub struct PeerCommand {
    pub session_id: SessionId,
    pub command_kind: PeerCommandKind,
}

#[derive(Debug, Clone)]
pub enum PeerCommandKind {
    SendMessage(VerifiedPeerMessage),
    Drop,
}

pub struct PeerService {
    config: PeerServiceConfig,
    peer_event_sender: Sender<PeerEvent>,
    command_receiver: Receiver<PeerCommand>,
    session_forwarders: Arc<Mutex<HashMap<SessionId, Sender<PeerCommand>>>>,
}

impl PeerService {
    pub fn new(
        config: PeerServiceConfig,
        peer_event_sender: Sender<PeerEvent>,
        command_receiver: Receiver<PeerCommand>,
    ) -> Result<Self> {
        Ok(PeerService {
            config,
            peer_event_sender,
            command_receiver,
            session_forwarders: Arc::new(Mutex::new(HashMap::new())),
        })
    }

    pub fn run(self) {
        scope(|s| {
            s.spawn(|_| {
                self.command_receiver.iter().for_each(|command| {
                    let session_id = command.session_id;
                    self.session_forwarders
                        .lock()
                        .unwrap()
                        .get(&session_id)
                        .map_or_else(
                            || {
                                info!("Forwarder of {} session is not found", session_id);
                                Ok(())
                            },
                            |forwarder| forwarder.send(command),
                        )
                        .map_err(|e| {
                            error!("Cannot forward message to {} session: {}", session_id, e)
                        })
                        .ok();
                })
            });
            self.config.dial_addresses.into_iter().for_each(|address| {
                debug!("Spawn thread to dial to {}", address);
                let event_sender = self.peer_event_sender.clone();
                let session_channels = Arc::clone(&self.session_forwarders);
                s.spawn(move |_| loop {
                    info!("Connecting to {}", address);
                    match TcpStream::connect(&address) {
                        Ok(stream) => Session::new(
                            stream,
                            event_sender.clone(),
                            Arc::clone(&session_channels),
                        )
                        .handle(),
                        Err(e) => {
                            error!(
                                "Cannot connect to {}, going to sleep {} millis: {}",
                                address,
                                self.config.dial_cooldown.as_millis(),
                                e
                            );
                            thread::sleep(self.config.dial_cooldown);
                        }
                    }
                });
            });
            let address = self
                .config
                .listen_address
                .unwrap_or_else(|| "127.0.0.1:0".to_owned());
            let listener = TcpListener::bind(address).unwrap();
            info!("Listening on {}", listener.local_addr().unwrap());
            listener.incoming().for_each(|it| match it {
                Ok(stream) => {
                    let event_sender = self.peer_event_sender.clone();
                    let session_channels = Arc::clone(&self.session_forwarders);
                    s.spawn(move |_| {
                        Session::new(stream, event_sender.clone(), session_channels).handle()
                    });
                }
                Err(e) => info!("Failed to accept connection: {}", e),
            });
        })
        .unwrap();
    }
}

fn read_stream(
    mut reader: BufReader<TcpStream>,
    event_sender: Sender<PeerEvent>,
    session_id: SessionId,
) -> Result<()> {
    let connected = event_sender
        .send(PeerEvent {
            session_id,
            event_kind: PeerEventKind::Connected,
        })
        .map_err(Error::from);

    let result = if connected.is_err() {
        (reader, connected)
    } else {
        let mut buffer = Vec::with_capacity(BUF_SIZE);
        loop {
            let mut take = reader.take(BUF_SIZE_U64);
            match take.read_until(b'\0', &mut buffer) {
                Ok(0) => break (take.into_inner(), Ok(())),
                Ok(n) => match serde_json::from_slice::<PeerMessage>(&buffer[..n - 1]) {
                    Ok(message) => {
                        debug!("Received message from {} peer: {:?}", session_id, message);
                        let result = message
                            .verified()
                            .map(|it| PeerEvent {
                                session_id,
                                event_kind: PeerEventKind::NewMessage(it),
                            })
                            .and_then(|it| event_sender.send(it).map_err(Error::from));
                        if result.is_err() {
                            break (take.into_inner(), result);
                        }
                        buffer.clear();
                        reader = take.into_inner();
                    }
                    Err(e) => {
                        debug!("Cannot parse message of {} session: {}", session_id, e);
                        break (take.into_inner(), Ok(()));
                    }
                },
                Err(e) => {
                    debug!("Failed to read stream of {} session: {}", session_id, e);
                    break (take.into_inner(), Ok(()));
                }
            }
        }
    };

    event_sender
        .send(PeerEvent {
            session_id,
            event_kind: PeerEventKind::Disconnected,
        })
        .map_err(|e| error!("{}: {}", session_id, e))
        .ok();

    shutdown(Ok(result.0.into_inner()), Shutdown::Read, session_id);

    result.1
}

fn write_stream(
    mut writer: BufWriter<TcpStream>,
    command_receiver: Receiver<PeerCommand>,
) -> Result<()> {
    for command in command_receiver {
        match command.command_kind {
            PeerCommandKind::SendMessage(message) => {
                debug!("Send message to {} peer", command.session_id);
                serde_json::to_writer(&mut writer, &PeerMessage::from(message))?;
                writer.write_all(&SEPARATOR)?;
                writer.flush()?;
            }
            PeerCommandKind::Drop => {
                shutdown(
                    writer.into_inner().map_err(Error::from),
                    Shutdown::Write,
                    command.session_id,
                );
                return Ok(());
            }
        }
    }
    Ok(())
}

fn shutdown(stream: Result<TcpStream>, mode: Shutdown, session_id: SessionId) {
    debug!("Shutdown {:?} of {} session", mode, session_id);
    stream
        .and_then(|it| it.shutdown(mode).map_err(Error::from))
        .map_err(|e| info!("{}: {}", session_id, e))
        .ok();
}

struct Session {
    id: SessionId,
    stream: TcpStream,
    event_sender: Sender<PeerEvent>,
    command_receiver: Receiver<PeerCommand>,
    session_forwarders: Arc<Mutex<HashMap<SessionId, Sender<PeerCommand>>>>,
}

impl Session {
    fn new(
        stream: TcpStream,
        event_sender: Sender<PeerEvent>,
        session_forwarders: Arc<Mutex<HashMap<SessionId, Sender<PeerCommand>>>>,
    ) -> Self {
        debug!("Creating session with {}", stream.peer_addr().unwrap());
        let id = random();
        stream.set_read_timeout(READ_WRITE_TIMEOUT).unwrap();
        stream.set_write_timeout(READ_WRITE_TIMEOUT).unwrap();
        let (forwarder, command_receiver) = bounded(1000);
        session_forwarders.lock().unwrap().insert(id, forwarder);
        Session {
            id,
            stream,
            event_sender,
            command_receiver,
            session_forwarders,
        }
    }

    fn handle(self) {
        info!(
            "Handling session {} with {}",
            self.id,
            self.stream.peer_addr().unwrap()
        );
        scope(|s| {
            let writer = BufWriter::with_capacity(BUF_SIZE, self.stream.try_clone().unwrap());
            s.spawn(|_| {
                write_stream(writer, self.command_receiver)
                    .map_err(|e| error!("Error writing to {}: {}", self.id, e))
                    .ok()
            });
            read_stream(
                BufReader::with_capacity(BUF_SIZE, self.stream),
                self.event_sender,
                self.id,
            )
            .map_err(|e| error!("Error reading from {}: {}", self.id, e))
            .ok();
        })
        .ok();
        self.session_forwarders.lock().unwrap().remove(&self.id);
    }
}
