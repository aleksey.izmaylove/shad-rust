#![forbid(unsafe_code)]

use std::cmp::Ordering::Equal;
use std::cmp::Ordering::Greater;
use std::cmp::Ordering::Less;

struct Node {
    key: Option<i64>,
    left_ptr: Option<Box<Node>>,
    right_ptr: Option<Box<Node>>,
    alive: bool,
}

impl Node {
    fn new(key: i64) -> Option<Box<Node>> {
        Some(Box::new(Node {
            key: Some(key),
            left_ptr: None,
            right_ptr: None,
            alive: true,
        }))
    }

    fn find(&self, key: &i64) -> Option<i64> {
        self.key.and_then(|probe| match key.cmp(&probe) {
            Equal => self.alive.then(|| probe),
            Less => self.left_ptr.as_ref().and_then(|node| node.find(key)),
            Greater => self.right_ptr.as_ref().and_then(|node| node.find(key)),
        })
    }

    fn insert(&mut self, key: i64) -> bool {
        match key.cmp(&self.key.unwrap()) {
            Equal => false,
            Less => match &mut self.left_ptr {
                Some(node) => node.insert(key),
                None => {
                    self.left_ptr = Node::new(key);
                    true
                }
            },
            Greater => match &mut self.right_ptr {
                Some(node) => node.insert(key),
                None => {
                    self.right_ptr = Node::new(key);
                    true
                }
            },
        }
    }

    fn remove(&mut self, key: &i64) -> Option<i64> {
        self.key.and_then(|probe| match key.cmp(&probe) {
            Equal => self.alive.then(|| {
                self.alive = false;
                probe
            }),
            Less => self.left_ptr.as_mut().and_then(|node| node.remove(key)),
            Greater => self.right_ptr.as_mut().and_then(|node| node.remove(key)),
        })
    }
}

#[derive(Default)]
pub struct BstSet {
    root: Option<Box<Node>>,
    length: usize,
}

impl BstSet {
    pub fn new() -> Self {
        BstSet::default()
    }

    pub fn len(&self) -> usize {
        self.length
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn contains(&self, key: i64) -> bool {
        match &self.root {
            Some(node) => node.find(&key).is_some(),
            None => false,
        }
    }

    /// If the key is already contained in set, return false.
    /// Otherwise insert the key and return true.
    pub fn insert(&mut self, key: i64) -> bool {
        if let Some(node) = &mut self.root {
            if node.insert(key) {
                self.length += 1;
                true
            } else {
                false
            }
        } else {
            self.root = Node::new(key);
            self.length += 1;
            true
        }
    }

    /// If the key is contained in set, remove it and return true.
    /// Otherwise return false.
    pub fn remove(&mut self, key: i64) -> bool {
        if let Some(node) = &mut self.root {
            if node.remove(&key).is_some() {
                self.length -= 1;
                return true;
            }
        }
        false
    }
}
