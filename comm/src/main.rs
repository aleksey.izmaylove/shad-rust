#![forbid(unsafe_code)]

use std::{
    collections::HashSet,
    env::args,
    fs::File,
    io::{BufRead, BufReader},
    process::exit,
};

fn main() {
    let args = args().collect::<Vec<String>>();
    if args.len() != 3 {
        println!("Specify 2 files");
        exit(1);
    }
    let first = args.get(1).unwrap();
    let last = args.get(2).unwrap();

    let mut set = match File::open(first) {
        Ok(file) => BufReader::new(file)
            .lines()
            .map(|line| line.unwrap())
            .collect::<HashSet<String>>(),
        Err(e) => {
            println!("Cannot open file: {} - {}", first, e);
            exit(1);
        }
    };
    match File::open(last) {
        Ok(file) => BufReader::new(file)
            .lines()
            .map(|line| line.unwrap())
            .for_each(|line| {
                if set.take(&line) != None {
                    println!("{}", line)
                }
            }),
        Err(e) => {
            println!("Cannot open file: {} - {}", last, e);
            exit(1);
        }
    }
}
