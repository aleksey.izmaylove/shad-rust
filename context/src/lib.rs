#![forbid(unsafe_code)]

use std::{
    any::{Any, TypeId},
    collections::HashMap,
    hash::Hash,
};

#[derive(Default)]
pub struct Context {
    type_map: HashMap<TypeId, Box<dyn Any>>,
    string_map: HashMap<String, Box<dyn Any>>,
}

impl Context {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn insert<K, V>(&mut self, key: K, obj: V)
    where
        K: Eq + Hash + AsRef<str>,
        V: Any,
    {
        self.string_map
            .insert(key.as_ref().to_string(), Box::new(obj));
    }

    pub fn get<T: Any>(&self, key: &str) -> &T {
        self.string_map.get(key).unwrap().downcast_ref().unwrap()
    }

    pub fn insert_singletone<T: Any>(&mut self, obj: T) {
        self.type_map.insert(obj.type_id(), Box::new(obj));
    }

    pub fn get_singletone<T: Any>(&self) -> &T {
        self.type_map
            .get(&TypeId::of::<T>())
            .unwrap()
            .downcast_ref()
            .unwrap()
    }
}
