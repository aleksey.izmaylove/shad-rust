#![forbid(unsafe_code)]

use futures::future::{self};
use futures::{stream, StreamExt};
use linkify::{LinkFinder, LinkKind};
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::task;

use std::collections::HashSet;

#[derive(Clone, Default)]
pub struct Config {
    pub concurrent_requests: Option<usize>,
}

#[derive(Debug)]
pub struct Page {
    pub url: String,
    pub body: String,
}

pub struct Crawler {
    config: Config,
}

const EVENT_QUEUE: usize = 1_000_000;

impl Crawler {
    pub fn new(config: Config) -> Self {
        Crawler { config }
    }

    pub fn run(&mut self, site: String) -> Receiver<Page> {
        let concurrency = self.config.concurrent_requests.unwrap_or(1);
        let (page_sender, page_receiver) = channel::<Page>(EVENT_QUEUE);
        task::spawn(Self::search_urls(site, page_sender, concurrency));
        page_receiver
    }

    async fn search_urls(site: String, page_sender: Sender<Page>, concurrency: usize) {
        let mut urls = Vec::from([site]);
        let mut visited = HashSet::<String>::new();
        while !urls.is_empty() {
            urls = stream::iter(urls)
                .filter(|url| future::ready(visited.insert(url.to_owned())))
                .map(|url| Self::get_page(url, &page_sender))
                .buffer_unordered(concurrency)
                .flat_map(stream::iter)
                .collect::<Vec<String>>()
                .await;
        }
    }

    async fn get_page(url: String, page_sender: &Sender<Page>) -> Vec<String> {
        let body = reqwest::get(&url).await.unwrap().text().await.unwrap();
        let mut finder = LinkFinder::new();
        finder.kinds(&[LinkKind::Url]);
        let links = finder
            .links(&body)
            .map(|l| l.as_str().to_owned())
            .collect::<Vec<String>>();
        page_sender.send(Page { url, body }).await.unwrap();
        links
    }
}
