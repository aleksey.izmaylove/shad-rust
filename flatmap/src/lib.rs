#![forbid(unsafe_code)]

use std::{borrow::Borrow, iter::FromIterator, ops::Index, vec::IntoIter};

#[derive(Default, Debug, PartialEq, Eq)]
pub struct FlatMap<K, V>(Vec<(K, V)>);

impl<K: Ord, V> FlatMap<K, V> {
    pub fn new() -> Self {
        FlatMap { 0: Vec::new() }
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn capacity(&self) -> usize {
        self.0.capacity()
    }

    pub fn as_slice(&self) -> &[(K, V)] {
        &self.0
    }

    /// Returns None if key was not present, or Some(prev_value) if it was.
    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        match self.0.binary_search_by(|pair| pair.0.cmp(&key)) {
            Ok(i) => {
                let prev = self.0.remove(i);
                self.0.insert(i, (key, value));
                Some(prev.1)
            }
            Err(i) => {
                self.0.insert(i, (key, value));
                None
            }
        }
    }

    pub fn get<Q>(&self, key: &Q) -> Option<&V>
    where
        K: Borrow<Q>,
        Q: Ord + ?Sized,
    {
        match self.0.binary_search_by(|pair| pair.0.borrow().cmp(key)) {
            Ok(i) => Some(&self.0.get(i).unwrap().1),
            Err(_) => None,
        }
    }

    pub fn remove<Q>(&mut self, key: &Q) -> Option<V>
    where
        K: Borrow<Q>,
        Q: Ord + ?Sized,
    {
        match self.0.binary_search_by(|pair| pair.0.borrow().cmp(key)) {
            Ok(i) => Some(self.0.remove(i).1),
            Err(_) => None,
        }
    }

    pub fn remove_entry<Q>(&mut self, key: &Q) -> Option<(K, V)>
    where
        K: Borrow<Q>,
        Q: Ord + ?Sized,
    {
        match self.0.binary_search_by(|pair| pair.0.borrow().cmp(key)) {
            Ok(i) => Some(self.0.remove(i)),
            Err(_) => None,
        }
    }

    fn dedup(&mut self) {
        self.0.sort_by(|a, b| b.0.cmp(&a.0));
        self.0.reverse();
        self.0.dedup_by(|a, b| a.0 == b.0);
    }
}

impl<K, Q, V> Index<&Q> for FlatMap<K, V>
where
    K: Ord + Borrow<Q>,
    Q: Ord + ?Sized,
{
    type Output = V;

    fn index(&self, index: &Q) -> &Self::Output {
        self.get(index).expect("No entry found for key")
    }
}

impl<K: Ord, V> Extend<(K, V)> for FlatMap<K, V> {
    fn extend<T: IntoIterator<Item = (K, V)>>(&mut self, iter: T) {
        self.0.extend(iter);
        self.dedup();
    }
}

impl<K: Ord, V> From<Vec<(K, V)>> for FlatMap<K, V> {
    fn from(vec: Vec<(K, V)>) -> Self {
        let mut map = FlatMap { 0: vec };
        map.dedup();
        map
    }
}

impl<K, V> From<FlatMap<K, V>> for Vec<(K, V)> {
    fn from(map: FlatMap<K, V>) -> Self {
        map.0
    }
}

impl<K: Ord, V> FromIterator<(K, V)> for FlatMap<K, V> {
    fn from_iter<T: IntoIterator<Item = (K, V)>>(iter: T) -> Self {
        let mut map = FlatMap {
            0: Vec::from_iter(iter),
        };
        map.dedup();
        map
    }
}

impl<K, V> IntoIterator for FlatMap<K, V> {
    type Item = (K, V);
    type IntoIter = IntoIter<(K, V)>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}
