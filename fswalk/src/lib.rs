#![forbid(unsafe_code)]

use std::{
    fs::{self, File},
    io::{self, BufReader, ErrorKind, Read},
    ops::DerefMut,
    path::Path,
};

////////////////////////////////////////////////////////////////////////////////

type Callback<'a> = dyn FnMut(&mut Handle) + 'a;

#[derive(Default)]
pub struct Walker<'a> {
    callbacks: Vec<Box<Callback<'a>>>,
}

impl<'a> Walker<'a> {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn add_callback<F>(&mut self, callback: F)
    where
        F: FnMut(&mut Handle) + 'a,
    {
        self.callbacks.push(Box::new(callback))
    }

    pub fn walk<P: AsRef<Path>>(&mut self, path: P) -> io::Result<()> {
        if self.callbacks.is_empty() {
            Ok(())
        } else {
            Walker::process_path(
                path,
                &mut self
                    .callbacks
                    .iter_mut()
                    .map(|fun| fun.deref_mut())
                    .collect::<Vec<_>>(),
            )
        }
    }

    fn process_dir<P: AsRef<Path>>(path: P, callbacks: &mut [&mut Callback]) -> io::Result<()> {
        let items = fs::read_dir(path)?;
        items.for_each(|it| {
            if let Ok(item) = it {
                Walker::process_path(item.path(), callbacks).ok();
            }
        });
        Ok(())
    }

    fn process_path<P: AsRef<Path>>(path: P, callbacks: &mut [&mut Callback]) -> io::Result<()> {
        let path = path.as_ref();
        let meta = path.metadata()?;
        if meta.is_file() {
            let interested = callbacks
                .iter_mut()
                .map(|fun| {
                    let mut handle = Handle::File(FileHandle::new(path));
                    fun(&mut handle);
                    (fun, handle)
                })
                .filter(|(_, handle)| match handle {
                    Handle::File(file_handle) => file_handle.interested,
                    _ => false,
                })
                .map(|(fun, _)| fun.deref_mut())
                .collect::<Vec<&mut Callback>>();

            if interested.is_empty() {
                Ok(())
            } else {
                let file = File::open(path)?;
                let mut buffer = Vec::new();
                BufReader::new(file).read_to_end(&mut buffer)?;
                let mut content_handle = Handle::Content {
                    file_path: path,
                    content: &buffer,
                };
                interested
                    .into_iter()
                    .for_each(|fun| fun(&mut content_handle));
                Ok(())
            }
        } else if meta.is_dir() {
            let mut interested = callbacks
                .iter_mut()
                .map(|fun| {
                    let mut handle = Handle::Dir(DirHandle::new(path));
                    fun(&mut handle);
                    (fun, handle)
                })
                .filter(|(_, handle)| match handle {
                    Handle::Dir(dir_handle) => dir_handle.interested,
                    _ => false,
                })
                .map(|(fun, _)| fun.deref_mut())
                .collect::<Vec<&mut Callback>>();
            if interested.is_empty() {
                Ok(())
            } else {
                Walker::process_dir(path, &mut interested[..])
            }
        } else {
            Err(ErrorKind::Other.into())
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

pub enum Handle<'a> {
    Dir(DirHandle<'a>),
    File(FileHandle<'a>),
    Content {
        file_path: &'a Path,
        content: &'a [u8],
    },
}

pub struct DirHandle<'a> {
    path: &'a Path,
    interested: bool,
}

impl<'a> DirHandle<'a> {
    pub fn descend(&mut self) {
        self.interested = true
    }

    pub fn path(&self) -> &Path {
        self.path
    }

    fn new(path: &'a Path) -> Self {
        Self {
            path,
            interested: false,
        }
    }
}

pub struct FileHandle<'a> {
    path: &'a Path,
    interested: bool,
}

impl<'a> FileHandle<'a> {
    pub fn read(&mut self) {
        self.interested = true;
    }

    pub fn path(&self) -> &Path {
        self.path
    }

    fn new(path: &'a Path) -> Self {
        Self {
            path,
            interested: false,
        }
    }
}
