use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput, Type};

#[proc_macro_derive(Scan)]
pub fn derive_scan(stream: TokenStream) -> TokenStream {
    let input: DeriveInput = parse_macro_input!(stream);
    let ident = input.ident;
    let empty_impl = quote! {
        impl ::gc::Scan for #ident {
            fn get_refs(&self) -> ::std::collections::HashSet<usize> {
                Default::default()
            }

            fn weak_count(&self) -> usize {
                0
            }
        }
    };
    match input.data {
        syn::Data::Struct(structure) => match structure.fields {
            syn::Fields::Named(fields) => fields
                .named
                .into_iter()
                .next()
                .filter(|field| {
                    if let Type::Path(path) = &field.ty {
                        let t_ident = path.path.segments.iter().next().unwrap().ident.to_string();
                        matches!(t_ident.as_ref(), "Option" | "Vec" | "RefCell")
                    } else {
                        false
                    }
                })
                .map(|field| {
                    let f_ident = field.ident;
                    let scan_impl = quote! {
                        impl ::gc::Scan for #ident {
                            fn get_refs(&self) -> ::std::collections::HashSet<usize> {
                                self.#f_ident.get_refs()
                            }

                            fn weak_count(&self) -> usize {
                                self.#f_ident.weak_count()
                            }
                        }
                    };
                    scan_impl.into()
                })
                .unwrap_or_else(|| empty_impl.into()),
            _ => empty_impl.into(),
        },
        _ => panic!(),
    }
}
