#![forbid(unsafe_code)]

pub use gc_derive::Scan;

use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    marker::PhantomData,
    ops::Deref,
    rc::{Rc, Weak},
};

////////////////////////////////////////////////////////////////////////////////

pub struct Gc<T: Scan> {
    pub weak: Weak<T>,
}

impl<T: Scan> Clone for Gc<T> {
    fn clone(&self) -> Self {
        Self {
            weak: self.weak.clone(),
        }
    }
}

impl<T: Scan> Gc<T> {
    pub fn borrow(&self) -> GcRef<'_, T> {
        GcRef {
            rc: self.weak.upgrade().unwrap(),
            lifetime: PhantomData,
        }
    }
}

pub struct GcRef<'a, T: Scan> {
    rc: Rc<T>,
    lifetime: PhantomData<&'a Gc<T>>,
}

impl<'a, T: Scan> Deref for GcRef<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.rc
    }
}

////////////////////////////////////////////////////////////////////////////////

pub trait Scan {
    fn get_refs(&self) -> HashSet<usize>;

    fn weak_count(&self) -> usize;
}

impl<T: Scan> Scan for Option<Gc<T>> {
    fn get_refs(&self) -> HashSet<usize> {
        self.as_ref()
            .map(|it| it.weak.as_ptr() as usize)
            .map(|it| HashSet::from([it]))
            .unwrap_or_default()
    }

    fn weak_count(&self) -> usize {
        self.as_ref()
            .map(|it| Weak::weak_count(&it.weak))
            .unwrap_or_default()
    }
}

impl<T: Scan> Scan for Vec<Gc<T>> {
    fn get_refs(&self) -> HashSet<usize> {
        self.iter().map(|it| it.weak.as_ptr() as usize).collect()
    }

    fn weak_count(&self) -> usize {
        self.iter().map(|it| Weak::weak_count(&it.weak)).sum()
    }
}

impl<T: Scan> Scan for RefCell<T> {
    fn get_refs(&self) -> HashSet<usize> {
        RefCell::borrow(self).get_refs()
    }

    fn weak_count(&self) -> usize {
        RefCell::borrow(self).weak_count()
    }
}

////////////////////////////////////////////////////////////////////////////////

pub struct Arena {
    objects: HashMap<usize, Rc<dyn Scan>>,
}

impl Arena {
    pub fn new() -> Self {
        Arena {
            objects: HashMap::new(),
        }
    }

    pub fn allocation_count(&self) -> usize {
        self.objects.len()
    }

    pub fn alloc<T: Scan + 'static>(&mut self, obj: T) -> Gc<T> {
        let rc = Rc::new(obj);
        let gc = Gc {
            weak: Rc::downgrade(&rc),
        };
        self.objects.insert(Rc::as_ptr(&rc) as usize, rc);
        gc
    }

    pub fn sweep(&mut self) {
        let inner_ref_counts = self
            .objects
            .iter()
            .flat_map(|(_, rc)| rc.get_refs())
            .filter(|r| self.objects.contains_key(r))
            .fold(HashMap::<usize, usize>::new(), |mut map, reference| {
                map.entry(reference)
                    .and_modify(|count| *count = count.checked_add(1).unwrap())
                    .or_insert(1);
                map
            });

        let external_refs = self
            .objects
            .iter()
            .filter(|(ptr, rc)| inner_ref_counts.get(ptr).unwrap_or(&0) < &Rc::weak_count(rc))
            .flat_map(|(ptr, _)| {
                let mut set = HashSet::new();
                self.mark(*ptr, &mut set);
                set
            })
            .collect::<HashSet<_>>();

        self.objects.retain(|k, _| external_refs.contains(k));
    }

    fn mark(&self, addr: usize, marked: &mut HashSet<usize>) {
        if !marked.contains(&addr) {
            marked.insert(addr);
            self.objects
                .get(&addr)
                .unwrap()
                .get_refs()
                .iter()
                .for_each(|it| {
                    self.mark(*it, marked);
                });
        }
    }
}

impl Default for Arena {
    fn default() -> Self {
        Self::new()
    }
}
