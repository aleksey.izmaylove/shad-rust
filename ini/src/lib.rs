#![forbid(unsafe_code)]

use std::collections::{hash_map::Entry, HashMap};

////////////////////////////////////////////////////////////////////////////////

pub type IniFile = HashMap<String, HashMap<String, String>>;

pub fn parse(content: &str) -> IniFile {
    let mut result = IniFile::new();
    let mut current_section = "";
    content
        .lines()
        .map(str::trim)
        .filter(valid_line)
        .for_each(|line| match as_section(line) {
            Some(section) => {
                assert_section(section);
                result
                    .entry(section.to_string())
                    .or_insert_with(HashMap::new);
                current_section = section;
            }
            None => {
                assert_section(line);
                match result.entry(current_section.to_string()) {
                    Entry::Occupied(mut map) => {
                        let kv = map.get_mut();
                        match line.split_once('=') {
                            Some((k, v)) => {
                                assert_key_value(v);
                                kv.insert(k.trim().to_string(), v.trim().to_string())
                            }
                            None => kv.insert(line.to_string(), String::new()),
                        };
                    }
                    Entry::Vacant(_) => panic!("Key-value without section."),
                }
            }
        });
    result
}

fn valid_line(s: &&str) -> bool {
    !s.is_empty()
}

fn as_section(s: &str) -> Option<&str> {
    if s.starts_with('[') && s.ends_with(']') {
        let mut chars = s.chars();
        chars.next();
        chars.next_back();
        Some(chars.as_str())
    } else {
        None
    }
}

fn assert_section(s: &str) {
    if s.contains('[') || s.contains(']') {
        panic!()
    }
}

fn assert_key_value(s: &str) {
    if s.contains('=') {
        panic!()
    }
}
