#![forbid(unsafe_code)]

use std::{cell::RefCell, collections::VecDeque, rc::Rc};

pub fn count() -> impl Iterator<Item = u64> {
    0..
}

pub fn cycle<T>(into_iter: T) -> impl Iterator<Item = T::Item>
where
    T: IntoIterator,
    T::Item: Clone,
{
    CycleIterator {
        source: into_iter.into_iter(),
        buffer: Vec::new(),
        index: 0,
        cycle: false,
    }
}

struct CycleIterator<T>
where
    T: Iterator,
    T::Item: Clone,
{
    source: T,
    buffer: Vec<T::Item>,
    index: usize,
    cycle: bool,
}

impl<T> CycleIterator<T>
where
    T: Iterator,
    T::Item: Clone,
{
    fn next_from_buffer(&mut self) -> Option<T::Item> {
        if self.index >= self.buffer.len() {
            self.index = 0;
        }
        let result = self.buffer.get(self.index).cloned();
        self.index += 1;
        result
    }
}

impl<T> Iterator for CycleIterator<T>
where
    T: Iterator,
    T::Item: Clone,
{
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cycle {
            self.next_from_buffer()
        } else {
            self.source
                .next()
                .map(|item| {
                    self.buffer.push(item.clone());
                    item
                })
                .or_else(|| {
                    self.cycle = true;
                    self.next_from_buffer()
                })
        }
    }
}

pub fn extract<T: IntoIterator>(
    into_iter: T,
    index: usize,
) -> (Option<T::Item>, impl Iterator<Item = T::Item>) {
    let mut rest = into_iter.into_iter();
    let mut observed = VecDeque::new();
    let mut i = 0;
    let item = loop {
        if let Some(item) = rest.next() {
            if i == index {
                break Some(item);
            } else {
                i += 1;
                observed.push_back(item);
            }
        } else {
            break None;
        }
    };
    (item, ExtractIterator { observed, rest })
}

struct ExtractIterator<T>
where
    T: Iterator,
{
    observed: VecDeque<T::Item>,
    rest: T,
}

impl<T> Iterator for ExtractIterator<T>
where
    T: Iterator,
{
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.observed.pop_front().or_else(|| self.rest.next())
    }
}

pub fn tee<T>(into_iter: T) -> (impl Iterator<Item = T::Item>, impl Iterator<Item = T::Item>)
where
    T: IntoIterator,
    T::Item: Clone,
{
    let source = Rc::new(RefCell::new(into_iter.into_iter()));
    let buffer = Rc::new(RefCell::new(VecDeque::new()));
    let following = Rc::new(RefCell::new(VecDeque::new()));
    let empty = Rc::new(RefCell::new(false));
    (
        TeeIterator {
            source: Rc::clone(&source),
            buffer: Rc::clone(&following),
            following: Rc::clone(&buffer),
            empty: Rc::clone(&empty),
        },
        TeeIterator {
            source,
            buffer,
            following,
            empty,
        },
    )
}

struct TeeIterator<T>
where
    T: Iterator,
    T::Item: Clone,
{
    source: Rc<RefCell<T>>,
    buffer: Rc<RefCell<VecDeque<T::Item>>>,
    following: Rc<RefCell<VecDeque<T::Item>>>,
    empty: Rc<RefCell<bool>>,
}

impl<T> Iterator for TeeIterator<T>
where
    T: Iterator,
    T::Item: Clone,
{
    type Item = T::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.buffer.borrow_mut().pop_front().or_else(|| {
            if *self.empty.borrow() {
                None
            } else {
                self.source
                    .borrow_mut()
                    .next()
                    .map(|item| {
                        self.following.borrow_mut().push_back(item.clone());
                        item
                    })
                    .or_else(|| {
                        RefCell::replace(&self.empty, true);
                        None
                    })
            }
        })
    }
}

pub fn group_by<T, F, V>(into_iter: T, f: F) -> impl Iterator<Item = (V, Vec<T::Item>)>
where
    T: IntoIterator,
    F: FnMut(&T::Item) -> V,
    V: Eq,
{
    GroupIterator {
        source: into_iter.into_iter(),
        fun: f,
        last: None,
        initial: true,
    }
}

struct GroupIterator<T, F, V>
where
    T: Iterator,
    F: FnMut(&T::Item) -> V,
    V: Eq,
{
    source: T,
    fun: F,
    last: Option<T::Item>,
    initial: bool,
}

impl<T, F, V> Iterator for GroupIterator<T, F, V>
where
    T: Iterator,
    F: FnMut(&T::Item) -> V,
    V: Eq,
{
    type Item = (V, Vec<T::Item>);

    fn next(&mut self) -> Option<Self::Item> {
        if self.initial {
            self.last = self.source.next();
            self.initial = false;
        }
        match self.last.take() {
            Some(last) => {
                let key = (self.fun)(&last);
                let mut group = vec![last];
                self.last = loop {
                    if let Some(next) = self.source.next() {
                        if (self.fun)(&next) == key {
                            group.push(next);
                        } else {
                            break Some(next);
                        }
                    } else {
                        break None;
                    }
                };
                Some((key, group))
            }
            None => None,
        }
    }
}
