#![forbid(unsafe_code)]

use std::{
    cell::RefCell,
    collections::VecDeque,
    fmt::Debug,
    ops::Deref,
    rc::{Rc, Weak},
};

use thiserror::Error;

#[derive(Error, Debug)]
#[error("channel is closed")]
pub struct SendError<T: Debug> {
    pub value: T,
}

pub struct Sender<T> {
    output: Weak<RefCell<VecDeque<T>>>,
}

impl<T: Debug> Sender<T> {
    pub fn send(&self, value: T) -> Result<(), SendError<T>> {
        if self.is_closed() {
            Err(SendError { value })
        } else {
            match self.output.upgrade() {
                Some(buffer) => {
                    buffer.deref().borrow_mut().push_back(value);
                    drop(buffer);
                    Ok(())
                }
                None => Err(SendError { value }),
            }
        }
    }

    pub fn is_closed(&self) -> bool {
        Weak::strong_count(&self.output) < 2
    }

    pub fn same_channel(&self, other: &Self) -> bool {
        self.output.ptr_eq(&other.output)
    }
}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Self {
        Sender {
            output: Weak::clone(&self.output),
        }
    }
}

#[derive(Error, Debug)]
pub enum ReceiveError {
    #[error("channel is empty")]
    Empty,
    #[error("channel is closed")]
    Closed,
}

pub struct Receiver<T> {
    input: Option<Rc<RefCell<VecDeque<T>>>>,
    backup: Rc<RefCell<VecDeque<T>>>,
}

impl<T> Receiver<T> {
    pub fn recv(&self) -> Result<T, ReceiveError> {
        match &self.input {
            Some(value) => {
                if Rc::weak_count(value) == 0 {
                    Err(ReceiveError::Closed)
                } else {
                    match value.deref().borrow_mut().pop_front() {
                        Some(value) => Ok(value),
                        None => Err(ReceiveError::Empty),
                    }
                }
            }
            None => match self.backup.borrow_mut().pop_front() {
                Some(value) => Ok(value),
                None => Err(ReceiveError::Closed),
            },
        }
    }

    pub fn close(&mut self) {
        self.input = None;
    }
}

impl<T> Drop for Receiver<T> {
    fn drop(&mut self) {
        self.close()
    }
}

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let queue = Rc::new(RefCell::default());
    (
        Sender {
            output: Rc::downgrade(&queue),
        },
        Receiver {
            backup: Rc::clone(&queue),
            input: Some(queue),
        },
    )
}
