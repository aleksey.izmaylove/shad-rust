use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput, Fields, FieldsNamed, LitStr};

#[proc_macro_derive(Object, attributes(table_name, column_name))]
pub fn derive_object(input: TokenStream) -> TokenStream {
    let DeriveInput {
        ident, data, attrs, ..
    } = parse_macro_input!(input);
    let table_name = attrs
        .iter()
        .find(|a| a.path.is_ident("table_name"))
        .map(|a| a.parse_args::<syn::LitStr>().ok())
        .flatten()
        .map_or(quote! {stringify!(#ident)}, |it| quote! {#it});
    let fields = match data {
        Data::Struct(structure) => match structure.fields {
            Fields::Named(FieldsNamed { named, .. }) => named
                .into_iter()
                .map(|f| {
                    (
                        f.ident.unwrap(),
                        f.attrs
                            .iter()
                            .find(|a| a.path.is_ident("column_name"))
                            .map(|a| a.parse_args::<LitStr>().ok())
                            .flatten(),
                        f.ty,
                    )
                })
                .collect::<Vec<_>>(),
            _ => Vec::new(),
        },
        _ => panic!(),
    };
    let field_name = fields.iter().map(|(it, _, _)| it).collect::<Vec<_>>();
    let column_name = fields
        .iter()
        .map(|(field, column, _)| {
            if let Some(name) = column {
                quote! {#name}
            } else {
                quote! {stringify!(#field)}
            }
        })
        .collect::<Vec<_>>();
    let field_type = fields.iter().map(|(_, _, it)| it);
    let out = quote! {
        impl Object for #ident {
            fn as_row(&self) -> orm::storage::Row {
                use orm::data::AsValue;
                vec![
                    #(self.#field_name.as_value()),*
                ]
            }
            fn from_row(mut row: orm::storage::Row, schema: &orm::object::Schema) -> Result<Box<Self>> {
                let row = &mut row;
                row.reverse();
                Ok(Box::new(Self {
                    #(#field_name: orm::object::try_column(row, schema, #column_name)?),*
                }))
            }
            fn schema() -> orm::object::Schema {
                use orm::data::AsDataType;
                orm::object::Schema::new(
                    stringify!(#ident),
                    #table_name,
                    vec![
                        #(orm::object::Column::new(stringify!(#field_name), #column_name, <#field_type>::DATA_TYPE)),*
                    ],
                )
            }
        }
    };
    out.into()
}
