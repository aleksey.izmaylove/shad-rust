use std::{
    borrow::Cow,
    fmt::{self, Display},
};

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct ObjectId(i64);

impl ObjectId {
    pub fn into_i64(&self) -> i64 {
        self.0
    }
}

impl From<ObjectId> for i64 {
    fn from(id: ObjectId) -> Self {
        id.0
    }
}

impl From<i64> for ObjectId {
    fn from(id: i64) -> Self {
        ObjectId(id)
    }
}

impl Display for ObjectId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum DataType {
    String,
    Bytes,
    Int64,
    Float64,
    Bool,
}

impl fmt::Display for DataType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

#[derive(Debug)]
pub enum Value<'a> {
    String(Cow<'a, str>),
    Bytes(Cow<'a, [u8]>),
    Int64(i64),
    Float64(f64),
    Bool(bool),
}

impl Value<'_> {
    pub fn as_data_type(&self) -> DataType {
        match self {
            Self::String(_) => DataType::String,
            Self::Bytes(_) => DataType::Bytes,
            Self::Int64(_) => DataType::Int64,
            Self::Float64(_) => DataType::Float64,
            Self::Bool(_) => DataType::Bool,
        }
    }
}

pub trait AsValue {
    fn as_value(&self) -> Value<'_>;
}

impl AsValue for String {
    fn as_value(&self) -> Value<'_> {
        Value::String(Cow::Borrowed(self))
    }
}

impl AsValue for Vec<u8> {
    fn as_value(&self) -> Value<'_> {
        Value::Bytes(Cow::Borrowed(self))
    }
}

impl AsValue for i64 {
    fn as_value(&self) -> Value<'_> {
        Value::Int64(*self)
    }
}

impl AsValue for f64 {
    fn as_value(&self) -> Value<'_> {
        Value::Float64(*self)
    }
}

impl AsValue for bool {
    fn as_value(&self) -> Value<'_> {
        Value::Bool(*self)
    }
}

impl TryFrom<Value<'_>> for String {
    type Error = ();

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::String(val) => Ok(val.to_string()),
            _ => Err(()),
        }
    }
}

impl TryFrom<Value<'_>> for Vec<u8> {
    type Error = ();

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::Bytes(val) => Ok(val.to_vec()),
            _ => Err(()),
        }
    }
}

impl TryFrom<Value<'_>> for i64 {
    type Error = ();

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::Int64(val) => Ok(val),
            _ => Err(()),
        }
    }
}

impl TryFrom<Value<'_>> for f64 {
    type Error = ();

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::Float64(val) => Ok(val),
            _ => Err(()),
        }
    }
}

impl TryFrom<Value<'_>> for bool {
    type Error = ();

    fn try_from(value: Value) -> Result<Self, Self::Error> {
        match value {
            Value::Bool(val) => Ok(val),
            _ => Err(()),
        }
    }
}

pub trait AsDataType {
    const DATA_TYPE: DataType;
}

impl AsDataType for String {
    const DATA_TYPE: DataType = DataType::String;
}

impl AsDataType for Vec<u8> {
    const DATA_TYPE: DataType = DataType::Bytes;
}

impl AsDataType for i64 {
    const DATA_TYPE: DataType = DataType::Int64;
}

impl AsDataType for f64 {
    const DATA_TYPE: DataType = DataType::Float64;
}

impl AsDataType for bool {
    const DATA_TYPE: DataType = DataType::Bool;
}
