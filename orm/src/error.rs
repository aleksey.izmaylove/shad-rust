use crate::{
    data::DataType,
    object::{Context, Schema},
    ObjectId,
};

use thiserror::Error;

const NO_ID: &str = "no such column: id";
const NO_COLUMN_NAMED: &str = "no column named ";

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    NotFound(Box<NotFoundError>),
    #[error(transparent)]
    UnexpectedType(Box<UnexpectedTypeError>),
    #[error(transparent)]
    MissingColumn(Box<MissingColumnError>),
    #[error("database is locked")]
    LockConflict,
    #[error("storage error: {0}")]
    Storage(#[source] Box<dyn std::error::Error>),
}

impl Error {
    pub(crate) fn from_context(error: rusqlite::Error, schema: &Schema, context: Context) -> Self {
        match error {
            rusqlite::Error::QueryReturnedNoRows => Error::not_found(context.id, schema),
            rusqlite::Error::InvalidColumnType(_, _, t) => {
                Error::unexpected_type(context, t.to_string())
            }
            rusqlite::Error::InvalidColumnName(_) => Error::missing_column(&context),
            e => Error::from_partial_context(e, schema),
        }
    }

    pub(crate) fn from_partial_context(error: rusqlite::Error, schema: &Schema) -> Self {
        match error {
            rusqlite::Error::SqliteFailure(e, Some(ref message)) => {
                if let rusqlite::ErrorCode::DatabaseBusy = e.code {
                    Error::LockConflict
                } else if message.contains(NO_ID) {
                    Error::MissingColumn(Box::new(MissingColumnError {
                        type_name: schema.type_name,
                        attr_name: "id",
                        table_name: schema.table,
                        column_name: "id",
                    }))
                } else if let Some((_, name)) = message.split_once(NO_COLUMN_NAMED) {
                    if let Some(column) = schema.columns.get(name) {
                        Error::MissingColumn(Box::new(MissingColumnError {
                            type_name: schema.type_name,
                            attr_name: column.attr_name,
                            table_name: schema.table,
                            column_name: column.column_name,
                        }))
                    } else {
                        error.into()
                    }
                } else {
                    error.into()
                }
            }
            e => e.into(),
        }
    }

    pub(crate) fn not_found(id: ObjectId, schema: &Schema) -> Error {
        Error::NotFound(Box::new(NotFoundError {
            object_id: id,
            type_name: schema.type_name,
        }))
    }

    pub(crate) fn missing_column(context: &Context) -> Self {
        Error::MissingColumn(Box::new(MissingColumnError {
            type_name: context.type_name,
            attr_name: context.attr_name,
            table_name: context.table_name,
            column_name: context.column_name,
        }))
    }

    pub(crate) fn unexpected_type(context: Context, got_type: String) -> Self {
        Error::UnexpectedType(Box::new(UnexpectedTypeError {
            type_name: context.type_name,
            attr_name: context.attr_name,
            table_name: context.table_name,
            column_name: context.column_name,
            expected_type: context.data_type,
            got_type,
        }))
    }
}

impl From<rusqlite::Error> for Error {
    fn from(error: rusqlite::Error) -> Self {
        Error::Storage(error.into())
    }
}

#[derive(Error, Debug)]
#[error("object is not found: type '{type_name}', id {object_id}")]
pub struct NotFoundError {
    pub object_id: ObjectId,
    pub type_name: &'static str,
}

#[derive(Error, Debug)]
#[error(
    "invalid type for {type_name}::{attr_name}: expected equivalent of {expected_type:?}, \
    got {got_type} (table: {table_name}, column: {column_name})"
)]
pub struct UnexpectedTypeError {
    pub type_name: &'static str,
    pub attr_name: &'static str,
    pub table_name: &'static str,
    pub column_name: &'static str,
    pub expected_type: DataType,
    pub got_type: String,
}

#[derive(Error, Debug)]
#[error(
    "missing a column for {type_name}::{attr_name} \
    (table: {table_name}, column: {column_name})"
)]
pub struct MissingColumnError {
    pub type_name: &'static str,
    pub attr_name: &'static str,
    pub table_name: &'static str,
    pub column_name: &'static str,
}

pub type Result<T> = std::result::Result<T, Error>;
