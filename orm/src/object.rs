use crate::{
    data::{DataType, Value},
    storage::Row,
    Error, ObjectId, Result,
};

use std::{any::Any, collections::HashMap};

pub trait Object: Any {
    fn as_row(&self) -> Row;
    fn from_row(row: Row, schema: &Schema) -> Result<Box<Self>>;
    fn schema() -> Schema;
}

pub struct Schema {
    pub(crate) type_name: &'static str,
    pub(crate) table: &'static str,
    pub(crate) columns: HashMap<&'static str, Column>,
    pub(crate) column_order: Vec<&'static str>,
}

impl Schema {
    pub fn new(type_name: &'static str, table: &'static str, columns: Vec<Column>) -> Self {
        let column_order = columns
            .iter()
            .map(|it| it.column_name)
            .collect::<Vec<&'static str>>();
        Schema {
            type_name,
            table,
            columns: columns.into_iter().map(|it| (it.column_name, it)).collect(),
            column_order,
        }
    }
}

#[derive(Debug)]
pub struct Column {
    pub attr_name: &'static str,
    pub column_name: &'static str,
    pub data_type: DataType,
}

impl Column {
    pub fn new(attr_name: &'static str, column_name: &'static str, data_type: DataType) -> Self {
        Column {
            attr_name,
            column_name,
            data_type,
        }
    }
}

pub fn try_column<'a, T>(row: &'a mut Row, schema: &Schema, name: &'static str) -> Result<T>
where
    T: TryFrom<Value<'a>>,
{
    let column = schema.columns.get(name).unwrap();
    let context = Context {
        type_name: schema.type_name,
        attr_name: column.attr_name,
        table_name: schema.table,
        column_name: column.column_name,
        data_type: column.data_type,
        id: 0.into(),
    };
    let val = row.pop().ok_or_else(|| Error::missing_column(&context))?;
    let got_type = val.as_data_type().to_string();
    val.try_into()
        .map_err(|_| Error::unexpected_type(context, got_type))
}

#[derive(Debug)]
pub(crate) struct Context {
    pub id: ObjectId,
    pub type_name: &'static str,
    pub attr_name: &'static str,
    pub table_name: &'static str,
    pub column_name: &'static str,
    pub data_type: DataType,
}
