use crate::{
    data::{DataType, Value},
    error::Result,
    object::{Context, Schema},
    Error, ObjectId,
};

use std::iter::once;

use rusqlite::{
    params_from_iter,
    types::{ToSqlOutput, Type},
    OptionalExtension, ToSql,
};

use std::borrow::Cow;

pub type Row<'a> = Vec<Value<'a>>;
pub type RowSlice<'a> = [Value<'a>];

pub(crate) trait StorageTransaction {
    fn table_exists(&self, table: &str) -> Result<bool>;
    fn create_table(&self, schema: &Schema) -> Result<()>;

    fn insert_row(&self, schema: &Schema, row: &RowSlice) -> Result<ObjectId>;
    fn update_row(&self, id: ObjectId, schema: &Schema, row: &RowSlice) -> Result<()>;
    fn select_row(&self, id: ObjectId, schema: &Schema) -> Result<Row<'static>>;
    fn delete_row(&self, id: ObjectId, schema: &Schema) -> Result<()>;

    fn commit(&self) -> Result<()>;
    fn rollback(&self) -> Result<()>;
}

impl<'a> StorageTransaction for rusqlite::Transaction<'a> {
    fn table_exists(&self, table: &str) -> Result<bool> {
        let sql = "select 1 from sqlite_master where name = ?";
        println!("{} {}", sql, table);
        self.query_row(sql, [table], |_| Ok(()))
            .optional()
            .map(|it| it.is_some())
            .map_err(Error::from)
    }

    fn create_table(&self, schema: &Schema) -> Result<()> {
        let sql_columns =
            once(("id", "Integer Primary Key Autoincrement".to_owned()))
                .chain(schema.columns.iter().map(|(_, column)| {
                    (column.column_name, column.data_type.sql_type().to_string())
                }))
                .map(|it| format!("{} {}", it.0, it.1))
                .collect::<Vec<_>>()
                .join(",\n  ");
        let sql = format!("create table {} (\n  {}\n)", schema.table, sql_columns);
        println!("{}", sql);
        self.execute_batch(sql.as_str())
            .map_err(|error| Error::from_partial_context(error, schema))
    }

    fn insert_row(&self, schema: &Schema, row: &RowSlice) -> Result<ObjectId> {
        let sql = if schema.columns.is_empty() {
            format!("insert into {} default values", schema.table)
        } else {
            let parameters = schema
                .column_order
                .iter()
                .map(|it| [":", it].concat())
                .collect::<Vec<String>>()
                .join(", ");
            format!(
                "insert into {}({}) values({})",
                schema.table,
                schema.column_order.join(", "),
                parameters
            )
        };
        println!("{} {:?}", sql, row);
        self.execute(&sql, params_from_iter(row))
            .map(|_| self.last_insert_rowid().into())
            .map_err(|error| Error::from_partial_context(error, schema))
    }

    fn update_row(&self, id: ObjectId, schema: &Schema, row: &RowSlice) -> Result<()> {
        let columns = schema
            .column_order
            .iter()
            .map(|it| [it, " = :", it].concat())
            .collect::<Vec<String>>()
            .join(", ");
        let sql = format!("update {} set {} where id = :id", schema.table, columns,);
        println!("{} {:?}", sql, row);
        self.execute(
            &sql,
            params_from_iter(row.iter().chain(once(&Value::Int64(id.into())))),
        )
        .map(|_| ())
        .map_err(|error| Error::from_partial_context(error, schema))
    }

    fn select_row(&self, id: ObjectId, schema: &Schema) -> Result<Row<'static>> {
        let sql = format!("select * from {} where id = ?", schema.table);
        println!("{} {}", sql, id);
        self.query_row(sql.as_str(), [id.into_i64()], |row| {
            Ok(schema
                .column_order
                .iter()
                .flat_map(|name| schema.columns.get(name))
                .map(|column| {
                    match column.data_type {
                        DataType::String => row
                            .get::<_, String>(column.column_name)
                            .map(Cow::Owned)
                            .map(Value::String),
                        DataType::Bytes => row
                            .get::<_, Vec<u8>>(column.column_name)
                            .map(Cow::Owned)
                            .map(Value::Bytes),
                        DataType::Int64 => row.get::<_, i64>(column.column_name).map(Value::Int64),
                        DataType::Float64 => {
                            row.get::<_, f64>(column.column_name).map(Value::Float64)
                        }
                        DataType::Bool => row.get::<_, bool>(column.column_name).map(Value::Bool),
                    }
                    .map_err(|error| {
                        Error::from_context(
                            error,
                            schema,
                            Context {
                                id,
                                type_name: schema.type_name,
                                attr_name: column.attr_name,
                                table_name: schema.table,
                                column_name: column.column_name,
                                data_type: column.data_type,
                            },
                        )
                    })
                })
                .partition::<Vec<Result<Value>>, _>(|it| it.is_ok()))
        })
        .map_err(|error| {
            Error::from_context(
                error,
                schema,
                Context {
                    id,
                    type_name: schema.type_name,
                    attr_name: "id",
                    table_name: schema.table,
                    column_name: "id",
                    data_type: DataType::Int64,
                },
            )
        })
        .and_then(|(ok, mut error)| {
            if error.is_empty() {
                Ok(ok.into_iter().map(|it| it.unwrap()).collect::<Vec<Value>>())
            } else {
                Err(error.pop().unwrap().unwrap_err())
            }
        })
    }

    fn delete_row(&self, id: ObjectId, schema: &Schema) -> Result<()> {
        let sql = format!("delete from {} where id = ?", schema.table);
        println!("{} {}", sql, id);
        self.execute(sql.as_str(), [id.into_i64()])
            .map(|_| ())
            .map_err(|error| Error::from_partial_context(error, schema))
    }

    fn commit(&self) -> Result<()> {
        println!("commit");
        self.execute_batch("COMMIT").map_err(Error::from)
    }

    fn rollback(&self) -> Result<()> {
        println!("rollback");
        self.execute_batch("ROLLBACK").map_err(Error::from)
    }
}

impl ToSql for Value<'_> {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        match self {
            Value::Bytes(val) => val.to_sql(),
            Value::Bool(val) => val.to_sql(),
            Value::Int64(val) => val.to_sql(),
            Value::Float64(val) => val.to_sql(),
            Value::String(val) => val.to_sql(),
        }
    }
}

trait AsSqlType {
    fn sql_type(&self) -> Type;
}

impl AsSqlType for DataType {
    fn sql_type(&self) -> Type {
        use rusqlite::types::Type::*;
        use DataType::*;
        match self {
            String => Text,
            Bytes => Blob,
            Int64 => Integer,
            Float64 => Real,
            Bool => Integer,
        }
    }
}
