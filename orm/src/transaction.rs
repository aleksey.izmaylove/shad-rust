use crate::{
    data::ObjectId,
    error::*,
    object::{Object, Schema},
    storage::{Row, StorageTransaction},
};

use std::{
    any::{Any, TypeId},
    cell::{Ref, RefCell, RefMut},
    collections::HashMap,
    marker::PhantomData,
    rc::Rc,
};

type ObjectCache = HashMap<ObjectId, Rc<RefCell<CacheItem>>>;

pub struct Transaction<'a> {
    inner: Box<dyn StorageTransaction + 'a>,
    cache: RefCell<HashMap<TypeId, ObjectCache>>,
}

impl<'a> Transaction<'a> {
    pub(crate) fn new(inner: Box<dyn StorageTransaction + 'a>) -> Self {
        Self {
            inner,
            cache: RefCell::new(HashMap::new()),
        }
    }

    pub fn create<T: Object>(&self, obj: T) -> Result<Tx<'_, T>> {
        let schema = &T::schema();
        if !self.inner.table_exists(schema.table)? {
            self.inner.create_table(schema)?
        }
        let id = self.inner.insert_row(schema, &obj.as_row())?;
        let item = Rc::new(RefCell::new(CacheItem {
            state: ObjectState::Clean,
            object: Box::new(obj),
        }));
        let tx = Tx {
            object: Rc::clone(&item),
            id,
            lifetime: PhantomData,
        };
        self.cache
            .borrow_mut()
            .entry(TypeId::of::<T>())
            .or_default()
            .insert(id, item);
        Ok(tx)
    }

    pub fn get<T: Object>(&self, id: ObjectId) -> Result<Tx<'_, T>> {
        let schema = &T::schema();
        if !self.inner.table_exists(schema.table)? {
            self.inner.create_table(schema)?
        }
        let mut type_cache = self.cache.borrow_mut();
        let object_cache = type_cache.entry(TypeId::of::<T>()).or_default();
        object_cache
            .get(&id)
            .map(Rc::clone)
            .map(|it| {
                let state = it.borrow().state;
                println!("get cache {} {:?}", id, state);
                match state {
                    ObjectState::Removed => Err(Error::not_found(id, schema)),
                    _ => Ok(it),
                }
            })
            .unwrap_or_else(|| {
                self.inner
                    .select_row(id, &T::schema())
                    .map(|row| T::from_row(row, schema))
                    .map(|it| {
                        let val = Rc::new(RefCell::new(CacheItem {
                            state: ObjectState::Clean,
                            object: it.unwrap(),
                        }));
                        object_cache.insert(id, Rc::clone(&val));
                        val
                    })
            })
            .map(|it| Tx {
                object: it,
                id,
                lifetime: PhantomData,
            })
    }

    pub fn commit(self) -> Result<()> {
        println!("before commit");
        self.cache
            .borrow()
            .iter()
            .map(|(_, v)| v.iter())
            .flatten()
            .map(|(key, val)| match &val.borrow().state {
                ObjectState::Modified => self.inner.update_row(
                    *key,
                    &val.borrow().object.schema(),
                    &val.borrow().object.as_row(),
                ),
                ObjectState::Removed => self.inner.delete_row(*key, &val.borrow().object.schema()),
                ObjectState::Clean => Ok(()),
            })
            .for_each(drop);
        self.inner.commit()
    }

    pub fn rollback(self) -> Result<()> {
        self.inner.rollback()
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ObjectState {
    Clean,
    Modified,
    Removed,
}

#[derive(Clone)]
pub struct Tx<'a, T> {
    object: Rc<RefCell<CacheItem>>,
    id: ObjectId,
    lifetime: PhantomData<&'a T>,
}

impl<'a, T: Any> Tx<'a, T> {
    pub fn id(&self) -> ObjectId {
        self.id
    }

    pub fn state(&self) -> ObjectState {
        self.object.borrow().state
    }

    pub fn borrow(&self) -> Ref<'_, T> {
        if let ObjectState::Removed = self.state() {
            panic!("cannot borrow a removed object")
        }
        Ref::map(self.object.borrow(), |it| {
            it.object.upcast().downcast_ref::<T>().unwrap()
        })
    }

    pub fn borrow_mut(&self) -> RefMut<'_, T> {
        if let ObjectState::Removed = self.state() {
            panic!("cannot borrow a removed object")
        }
        self.object.borrow_mut().state = ObjectState::Modified;
        RefMut::map(self.object.borrow_mut(), |it| {
            it.object.upcast_mut().downcast_mut::<T>().unwrap()
        })
    }

    pub fn delete(self) {
        println!("delete");
        self.object
            .try_borrow_mut()
            .expect("cannot delete a borrowed object")
            .state = ObjectState::Removed;
    }
}

trait CacheableObject {
    fn schema(&self) -> Schema;
    fn as_row(&self) -> Row;
    fn upcast(&self) -> &dyn Any;
    fn upcast_mut(&mut self) -> &mut dyn Any;
}

impl<T: Object> CacheableObject for T {
    fn as_row(&self) -> Row {
        self.as_row()
    }

    fn schema(&self) -> Schema {
        T::schema()
    }

    fn upcast(&self) -> &dyn Any {
        self
    }

    fn upcast_mut(&mut self) -> &mut dyn Any {
        self
    }
}

struct CacheItem {
    state: ObjectState,
    object: Box<dyn CacheableObject>,
}
