#![forbid(unsafe_code)]

use std::{
    fs::{self, File},
    io::{self, BufRead, BufReader},
    iter::once,
    path::{Path, PathBuf},
};

use rayon::prelude::*;

////////////////////////////////////////////////////////////////////////////////

#[derive(Debug, PartialEq, Eq)]
pub struct Match {
    pub path: PathBuf,
    pub line: String,
    pub line_number: usize,
}

#[derive(Debug)]
pub struct Error {
    pub path: PathBuf,
    pub error: io::Error,
}

pub enum Event {
    Match(Match),
    Error(Error),
}

pub fn run<P: AsRef<Path>>(path: P, pattern: &str) -> Vec<Event> {
    let path = path.as_ref().to_path_buf();
    match path.metadata() {
        Ok(_) => find_files(path)
            .into_par_iter()
            .flat_map(|path| scan_file(path, pattern).into_par_iter())
            .collect::<Vec<Event>>(),
        Err(error) => vec![Event::Error(Error { path, error })],
    }
}

fn scan_file<P: AsRef<Path>>(path: P, pattern: &str) -> Vec<Event> {
    let path_buf = path.as_ref().to_path_buf();
    match File::open(path) {
        Ok(file) => BufReader::new(file)
            .lines()
            .zip(1..)
            .flat_map(|(result, line_number)| {
                result
                    .into_iter()
                    .filter(|line| line.contains(pattern))
                    .zip(once(path_buf.clone()))
                    .map(move |(line, path)| {
                        Event::Match(Match {
                            path,
                            line,
                            line_number,
                        })
                    })
            })
            .collect(),
        Err(error) => vec![Event::Error(Error {
            path: path_buf,
            error,
        })],
    }
}

fn find_files<P: AsRef<Path>>(path: P) -> Vec<PathBuf> {
    let path_ref = path.as_ref().to_path_buf();
    if path_ref.is_file() {
        vec![path_ref]
    } else if path_ref.is_dir() {
        scan_dir(path_ref)
    } else {
        Vec::new()
    }
}

fn scan_dir<P: AsRef<Path>>(path: P) -> Vec<PathBuf> {
    fs::read_dir(path)
        .into_iter()
        .flat_map(|dir| {
            dir.into_iter()
                .flat_map(|entry| entry.into_iter().map(|it| it.path()).flat_map(find_files))
        })
        .collect()
}
