#![forbid(unsafe_code)]

use std::{collections::HashMap, fmt::Display};

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Number(f64),
    Symbol(String),
}

enum Op {
    Plus,
    Minus,
    Multi,
    Div,
}
enum Action {
    Op(Op),
    Value(Value),
    Set,
}

enum Var {
    Ref(String),
    Def(String),
}

#[derive(Default)]
pub struct Interpreter {
    stack: Vec<Value>,
    vars: HashMap<String, Value>,
}

impl Interpreter {
    pub fn new() -> Self {
        Interpreter {
            ..Default::default()
        }
    }

    pub fn eval(&mut self, expr: &str) {
        expr.trim()
            .split_whitespace()
            .map(|s| match &self.get_action(s) {
                Action::Value(value) => {
                    self.stack.push(value.clone());
                    Ok(s)
                }
                Action::Op(op) => {
                    if let Value::Number(x) = self.stack.pop().unwrap() {
                        if let Value::Number(y) = self.stack.pop().unwrap() {
                            let result = match op {
                                Op::Plus => x + y,
                                Op::Minus => x - y,
                                Op::Multi => x * y,
                                Op::Div => x / y,
                            };
                            self.stack.push(Value::Number(result));
                            return Ok(s);
                        }
                    };
                    Err("Not a number")
                }
                Action::Set => {
                    if let Value::Symbol(key) = self.stack.pop().unwrap() {
                        let value = self.stack.pop().unwrap();
                        self.vars.insert(key, value);
                        Ok(s)
                    } else {
                        Err("Not a symbol")
                    }
                }
            })
            .for_each(|it| {
                if let Err(e) = it {
                    panic!("{}", e)
                }
            });
    }

    pub fn stack(&self) -> &[Value] {
        &self.stack[..]
    }

    fn get_action(&self, s: &str) -> Action {
        match s {
            "+" => Action::Op(Op::Plus),
            "-" => Action::Op(Op::Minus),
            "*" => Action::Op(Op::Multi),
            "/" => Action::Op(Op::Div),
            "set" => Action::Set,
            _ => match get_var(s) {
                Some(var) => match var {
                    Var::Def(s) => Action::Value(Value::Symbol(s)),
                    Var::Ref(s) => Action::Value(self.vars.get(&s).unwrap().clone()),
                },
                None => match s.parse::<f64>() {
                    Ok(number) => Action::Value(Value::Number(number)),
                    Err(e) => panic!("{}", e),
                },
            },
        }
    }
}

fn get_var(s: &str) -> Option<Var> {
    if s.len() > 1 {
        if let Some(val) = s.strip_prefix('\'') {
            return Some(Var::Def(val.to_string()));
        }
        if let Some(val) = s.strip_prefix('$') {
            return Some(Var::Ref(val.to_string()));
        }
    }
    None
}

impl Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            Value::Number(x) => write!(f, "{}", x),
            Value::Symbol(x) => write!(f, "{}", x),
        }
    }
}
