#![forbid(unsafe_code)]

use std::rc::Rc;

////////////////////////////////////////////////////////////////////////////////

pub struct PRef<T> {
    value: Rc<T>,
    next: Option<Rc<PRef<T>>>,
}

impl<T> std::ops::Deref for PRef<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl<T> PRef<T> {
    fn copy(pref: &Option<Rc<PRef<T>>>) -> Option<Rc<PRef<T>>> {
        pref.as_ref().map(|next| Rc::clone(next))
    }
}

////////////////////////////////////////////////////////////////////////////////

pub struct PStack<T> {
    head: Option<Rc<PRef<T>>>,
    len: usize,
}

impl<T> Default for PStack<T> {
    fn default() -> Self {
        PStack { head: None, len: 0 }
    }
}

impl<T> Clone for PStack<T> {
    fn clone(&self) -> Self {
        PStack {
            head: PRef::copy(&self.head),
            len: self.len,
        }
    }
}

impl<T> PStack<T> {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn push(&self, value: T) -> Self {
        PStack {
            head: Some(Rc::new(PRef {
                value: Rc::new(value),
                next: PRef::copy(&self.head),
            })),
            len: self.len + 1,
        }
    }

    pub fn pop(&self) -> Option<(PRef<T>, Self)> {
        self.head.as_ref().map(|head| {
            (
                PRef {
                    value: Rc::clone(&head.value),
                    next: None,
                },
                PStack {
                    head: PRef::copy(&head.next),
                    len: self.len - 1,
                },
            )
        })
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    pub fn iter(&self) -> impl Iterator<Item = PRef<T>> {
        PStackIter {
            cursor: PRef::copy(&self.head),
        }
    }
}

struct PStackIter<T> {
    cursor: Option<Rc<PRef<T>>>,
}

impl<T> Iterator for PStackIter<T> {
    type Item = PRef<T>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.cursor {
            Some(ref pref) => {
                let result = PRef {
                    value: Rc::clone(&pref.value),
                    next: None,
                };
                self.cursor = PRef::copy(&pref.next);
                Some(result)
            }
            _ => None,
        }
    }
}
