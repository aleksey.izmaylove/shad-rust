#![forbid(unsafe_code)]

use std::{
    collections::{HashMap, HashSet, VecDeque},
    convert::TryInto,
    iter,
    rc::Rc,
    usize,
};

pub const SIZE: usize = 3;

////////////////////////////////////////////////////////////////////////////////

/// Represents a tile on a board. A tile can either be empty or a number from 1 to 8.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Tile(u8);

impl Tile {
    /// Creates a new tile.
    ///
    /// # Arguments
    ///
    /// * `maybe_value` - Some(1..=8) or None.
    ///
    /// # Panics
    ///
    /// Panics if value is 0 or > 8.
    pub fn new(maybe_value: Option<u8>) -> Self {
        match maybe_value {
            Some(value) => {
                assert!(value >= 1 && value <= 8, "invalid tile value: {}", value);
                return Tile(value);
            }
            None => Tile(9),
        }
    }

    /// Creates an empty tile.
    pub fn empty() -> Self {
        return Tile(9);
    }

    /// Returns `Some(value)` if tile contains a value, otherwise returns `None`.
    pub fn number(&self) -> Option<u8> {
        if self.0 == 9 {
            return None;
        }
        return Some(self.0);
    }

    /// Returns true if tile does not contain a value.
    pub fn is_empty(&self) -> bool {
        self.0 == 9
    }
}

////////////////////////////////////////////////////////////////////////////////

/// Represents a 3x3 board of tiles.
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Board {
    tiles: [[Tile; 3]; 3],
    space: (usize, usize),
}

impl Board {
    /// Creates a new `Board` from a 3x3 matrix if `Tile`s.
    ///
    /// # Panics
    ///
    /// Panics if `tiles` contains more than one instance of some tile.
    pub fn new(tiles: [[Tile; 3]; 3]) -> Self {
        let map = tiles
            .iter()
            .enumerate()
            .flat_map(|(index, line)| line.iter().zip(CoordinateCounter::new(index)))
            .collect::<HashMap<_, _>>();
        assert!(map.len() == 9, "Duplicated values");
        let space = (*map.get(&Tile::empty()).unwrap()).point;
        return Board { tiles, space };
    }

    /// Returns a tile on a given `row` and `col`.
    ///
    /// # Panics
    ///
    /// Panics if `row` or `col` > 2.
    pub fn get(&self, row: usize, col: usize) -> Tile {
        match self.tiles.get(row).and_then(|line| line.get(col)) {
            Some(value) => value.clone(),
            None => panic!("Tile not found"),
        }
    }

    /// Swaps two given tiles.
    ///
    /// # Panics
    ///
    /// Panics if some of `r1`, `r2`, `c1` or `c2` > 2.
    pub fn swap(&mut self, r1: usize, c1: usize, r2: usize, c2: usize) {
        let buf = self.tiles[r1][c1].clone();
        self.tiles[r1][c1] = self.tiles[r2][c2].clone();
        self.tiles[r2][c2] = buf;
        if self.space == (r1, c1) {
            self.space = (r2, c2)
        } else if self.space == (r2, c2) {
            self.space = (r1, c1)
        }
    }

    /// Parses `Board` from string.
    ///
    /// # Arguments
    ///
    /// * `s` must be a string in the following format:
    ///
    /// '''
    /// .12
    /// 345
    /// 678
    /// '''
    ///
    /// # Panics
    ///
    /// Panics of `s` is the wrong format or does not represent a valid `Board`.
    pub fn from_string(s: &str) -> Self {
        let tiles = s
            .lines()
            .map(|line| {
                line.chars()
                    .map(|c| {
                        if c.is_numeric() {
                            Tile::new(Some(c.to_digit(10).unwrap() as u8))
                        } else if c == '.' {
                            Tile::empty()
                        } else {
                            panic!("Unexpected symbol {} ({})", c, c as usize)
                        }
                    })
                    .collect::<Vec<Tile>>()
                    .try_into()
                    .unwrap_or_else(|_| panic!("Line size is not {}", SIZE))
            })
            .collect::<Vec<[Tile; SIZE]>>()
            .try_into()
            .unwrap();
        Board::new(tiles)
    }

    /// Returns a string representation of this board in the following format:
    ///
    /// '''
    /// .12
    /// 345
    /// 678
    /// '''
    pub fn to_string(&self) -> String {
        self.tiles
            .iter()
            .flat_map(|line| {
                line.iter()
                    .map(|tile| {
                        tile.number()
                            .and_then(|number| Some(number.to_string()))
                            .unwrap_or('.'.to_string())
                    })
                    .chain(iter::once('\n'.to_string()))
            })
            .collect()
    }

    fn is_solved(self) -> bool {
        for (tile, i) in self.tiles.iter().flatten().zip(1..) {
            let value = tile.number().unwrap_or(9) as usize;
            if value != i {
                return false;
            }
        }
        return true;
    }

    fn if_solvable(self) -> Option<Board> {
        let numbers = self
            .tiles
            .iter()
            .flatten()
            .map(|tile| tile.number())
            .filter(|number| number.is_some())
            .map(|number| number.unwrap())
            .collect::<Vec<u8>>();
        let mut inversions = 0;
        for i in 0..(numbers.len() - 1) {
            for j in (i + 1)..numbers.len() {
                if numbers.get(i).unwrap() > numbers.get(j).unwrap() {
                    inversions += 1;
                }
            }
        }
        if inversions % 2 == 0 {
            Some(self)
        } else {
            None
        }
    }

    fn swap_space(&self, tile: (usize, usize)) -> Board {
        let mut new_board = self.clone();
        new_board.swap(new_board.space.0, new_board.space.1, tile.0, tile.1);
        new_board
    }
}

////////////////////////////////////////////////////////////////////////////////

/// Returns the shortest sequence of moves that solves this board.
/// That is, a sequence of boards such that each consecutive board can be obtained from
/// the previous one via a single swap of an empty tile with some adjacent tile,
/// and the final board in the sequence is
///
/// '''
/// 123
/// 456
/// 78.
/// '''
///
/// If the board is unsolvable, returns `None`. If the board is already solved,
/// returns an empty vector.
pub fn solve(start: Board) -> Option<Vec<Board>> {
    let mut queue = VecDeque::new();
    let mut visited = HashSet::new();
    start
        .if_solvable()
        .and_then(|board| loop {
            let node = queue.pop_front().unwrap_or_else(|| Node::root(board));
            if node.board.is_solved() {
                break Some(node);
            }
            node.find_moves()
                .into_iter()
                .filter(|child| visited.insert(child.board))
                .for_each(|child| queue.push_back(child));
        })
        .and_then(|node| {
            let mut path = Vec::new();
            let mut cursor = &node;
            while cursor.parent.is_some() {
                path.push(cursor.board);
                cursor = cursor.parent.as_ref().unwrap();
            }
            path.reverse();
            Some(path)
        })
}

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
struct Node {
    board: Board,
    parent: Option<Rc<Node>>,
}

impl Node {
    fn root(board: Board) -> Node {
        Node {
            board,
            parent: None,
        }
    }

    fn child(parent: Rc<Node>, board: Board) -> Node {
        Node {
            board,
            parent: Some(parent),
        }
    }

    fn find_moves(self) -> Vec<Node> {
        let board = self.board;
        let (row, column) = board.space;
        let parent = Rc::new(self);
        let mut children = Vec::with_capacity(4);
        if row > 0 {
            children.push(Node::child(
                parent.clone(),
                board.swap_space((row - 1, column)),
            ));
        }
        if column < SIZE - 1 {
            children.push(Node::child(
                parent.clone(),
                board.swap_space((row, column + 1)),
            ));
        }
        if row < SIZE - 1 {
            children.push(Node::child(
                parent.clone(),
                board.swap_space((row + 1, column)),
            ));
        }
        if column > 0 {
            children.push(Node::child(
                parent.clone(),
                board.swap_space((row, column - 1)),
            ));
        }
        return children;
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
struct CoordinateCounter {
    point: (usize, usize),
    init: bool,
}

impl Iterator for CoordinateCounter {
    type Item = CoordinateCounter;

    fn next(&mut self) -> Option<Self::Item> {
        self.point.1 += if self.init {
            self.init = false;
            0
        } else {
            1
        };
        Some(*self)
    }
}

impl CoordinateCounter {
    fn new(static_dimension: usize) -> Self {
        CoordinateCounter {
            point: (static_dimension, 0),
            init: true,
        }
    }
}
