#![forbid(unsafe_code)]

use puzzle::SIZE;

fn main() {
    let mut input = String::new();
    for _ in 0..SIZE {
        std::io::stdin().read_line(&mut input).unwrap();
    }

    let board = puzzle::Board::from_string(&input);
    if let Some(moves) = puzzle::solve(board) {
        if moves.is_empty() {
            println!("Already solved");
        }
        for mv in moves {
            print!("---\n{}", mv.to_string());
        }
    } else {
        println!("No solution.");
    }
}
