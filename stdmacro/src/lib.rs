#![forbid(unsafe_code)]

#[macro_export]
macro_rules! deque {
    () => {
        ::std::collections::VecDeque::new()
    };
    ($($item:expr),+ $(,)?) => {
        IntoIterator::into_iter([$($item),+]).collect::<::std::collections::VecDeque<_>>()
    };
    ($item:expr; $count:expr) => {
        IntoIterator::into_iter([$item; $count]).collect::<::std::collections::VecDeque<_>>()
    };
}

#[macro_export]
macro_rules! sorted_vec {
    () => {
        ::std::vec::Vec::new()
    };
    ($($item:expr),+ $(,)?) => {{
        let mut vec = ::std::vec::Vec::from([$($item),+]);
        vec.sort();
        vec
    }};
}

#[macro_export]
macro_rules! map {
    () => {
        ::std::collections::HashMap::new()
    };
    ($($key:expr => $value:expr),+ $(,)?) => {{
        let mut map = ::std::collections::HashMap::new();
        for (k,v) in [$(($key, $value)),+] {
            map.insert(k,v);
        }
        map
    }};
}
