#![forbid(unsafe_code)]

use std::net::{IpAddr, Ipv4Addr, Shutdown, SocketAddr, TcpListener, TcpStream, ToSocketAddrs};
use std::sync::Arc;
use std::thread;
use std::time::Duration;

use log::info;
use std::io::{prelude::*, BufReader, BufWriter, Result};

const READ_WRITE_TIMEOUT: Option<Duration> = Some(Duration::from_secs(10));
const BUF_SIZE_BYTES: usize = 1024;

pub fn run_proxy(port: u32, destination: String) {
    let dest = Arc::new(destination);
    let listener = TcpListener::bind(SocketAddr::new(
        IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
        port.try_into().unwrap(),
    ))
    .unwrap();
    loop {
        match listener.accept() {
            Ok((stream, peer)) => {
                info!("Downstream connection accepted: {}", peer);
                let dest = Arc::clone(&dest);
                thread::spawn(move || proxy(stream, dest));
            }
            Err(e) => info!("Failed to accept downstream connection: {}", e),
        }
    }
}

fn proxy<A>(downstream: TcpStream, destination: Arc<A>) -> Result<()>
where
    A: ToSocketAddrs,
{
    downstream.set_read_timeout(READ_WRITE_TIMEOUT).unwrap();
    downstream.set_write_timeout(READ_WRITE_TIMEOUT).unwrap();
    let upstream = TcpStream::connect(destination.as_ref())?;
    upstream
        .peer_addr()
        .map(|it| info!("Connected to upstream: {}", it))
        .ok();
    upstream.set_read_timeout(READ_WRITE_TIMEOUT).unwrap();
    upstream.set_write_timeout(READ_WRITE_TIMEOUT).unwrap();
    let down_reader = BufReader::with_capacity(BUF_SIZE_BYTES, downstream.try_clone().unwrap());
    let up_writer = BufWriter::with_capacity(BUF_SIZE_BYTES, upstream.try_clone().unwrap());
    thread::spawn(move || transmit(down_reader, up_writer, "up"));
    transmit(
        BufReader::with_capacity(BUF_SIZE_BYTES, upstream),
        BufWriter::with_capacity(BUF_SIZE_BYTES, downstream),
        "down",
    )
    .map(|()| info!("Complete session"))
}

fn transmit(
    mut reader: BufReader<TcpStream>,
    mut writer: BufWriter<TcpStream>,
    direction: &str,
) -> Result<()> {
    while let Ok(buffer) = reader.fill_buf() {
        let lenght = buffer.len();
        if lenght == 0 {
            info!("Connection closed - {}", direction);
            break;
        } else {
            writer.write_all(buffer).and(writer.flush()).map(|()| {
                info!("Transmitted {} bytes - {}", lenght, direction);
                reader.consume(lenght);
            })?;
        }
    }
    writer.into_inner()?.shutdown(Shutdown::Write)
}
