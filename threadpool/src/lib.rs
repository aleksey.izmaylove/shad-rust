#![forbid(unsafe_code)]

use crossbeam::channel::{self, Receiver, Sender};

use std::{
    panic::{catch_unwind, AssertUnwindSafe},
    thread,
};

pub struct ThreadPool {
    task_sender: Sender<Box<dyn FnOnce(usize) + Send + Sync>>,
}

impl ThreadPool {
    pub fn new(thread_count: usize, queue_size: usize) -> Self {
        let (task_sender, task_receiver) =
            channel::bounded::<Box<dyn FnOnce(usize) + Send + Sync>>(queue_size);
        for i in 0..thread_count {
            let receiver = task_receiver.clone();
            thread::spawn(move || loop {
                while let Ok(fun) = receiver.recv() {
                    fun(i);
                }
            });
        }
        ThreadPool { task_sender }
    }

    pub fn spawn<F, T>(&self, task: F) -> JoinHandle<T>
    where
        F: FnOnce() -> T + Send + Sync + 'static,
        T: Send + Sync + 'static,
    {
        let (result_sender, result_receiver) = channel::bounded(1);
        let fun = move |_thread: usize| {
            let result = catch_unwind(AssertUnwindSafe(task));
            result_sender.try_send(result).ok();
        };
        self.task_sender.send(Box::new(fun)).unwrap();
        JoinHandle { result_receiver }
    }

    pub fn shutdown(self) {
        while !self.task_sender.is_empty() {}
        drop(self.task_sender);
    }
}

pub struct JoinHandle<T> {
    result_receiver: Receiver<thread::Result<T>>,
}

#[derive(Debug)]
pub struct JoinError {}

impl<T> JoinHandle<T> {
    pub fn join(self) -> Result<T, JoinError> {
        self.result_receiver
            .recv()
            .map_err(|_| JoinError {})?
            .map_err(|_| JoinError {})
    }
}
