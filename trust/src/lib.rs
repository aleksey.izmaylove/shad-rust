#![forbid(unsafe_code)]

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum RoundOutcome {
    BothCooperated,
    LeftCheated,
    RightCheated,
    BothCheated,
}

pub struct Game {
    left: Box<dyn Trait>,
    right: Box<dyn Trait>,

    left_score: i32,
    right_score: i32,
}

impl Game {
    pub fn new(left: Box<dyn Trait>, right: Box<dyn Trait>) -> Self {
        Game {
            left,
            right,
            left_score: 0,
            right_score: 0,
        }
    }

    pub fn left_score(&self) -> i32 {
        self.left_score
    }

    pub fn right_score(&self) -> i32 {
        self.right_score
    }

    pub fn play_round(&mut self) -> RoundOutcome {
        let pair = (self.left.play(), self.right.play());
        let result = match pair {
            (Action::Coop, Action::Coop) => {
                self.left_score += 2;
                self.right_score += 2;
                RoundOutcome::BothCooperated
            }
            (Action::Cheat, Action::Coop) => {
                self.left_score += 3;
                self.right_score -= 1;
                RoundOutcome::LeftCheated
            }
            (Action::Coop, Action::Cheat) => {
                self.left_score -= 1;
                self.right_score += 3;
                RoundOutcome::RightCheated
            }
            (Action::Cheat, Action::Cheat) => RoundOutcome::BothCheated,
        };
        self.left.react(pair.1);
        self.right.react(pair.0);
        result
    }
}

pub trait Trait {
    fn play(&self) -> Action;

    fn react(&mut self, opponent_action: Action);
}

#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Action {
    Coop,
    Cheat,
}

#[derive(Default)]
pub struct CheatingAgent {}

impl Trait for CheatingAgent {
    fn play(&self) -> Action {
        Action::Cheat
    }

    fn react(&mut self, _: Action) {}
}

#[derive(Default)]
pub struct CooperatingAgent {}

impl Trait for CooperatingAgent {
    fn play(&self) -> Action {
        Action::Coop
    }

    fn react(&mut self, _: Action) {}
}

#[derive(Default)]
pub struct GrudgerAgent {
    was_cheated: bool,
}

impl Trait for GrudgerAgent {
    fn play(&self) -> Action {
        if self.was_cheated {
            Action::Cheat
        } else {
            Action::Coop
        }
    }

    fn react(&mut self, opponent_action: Action) {
        if Action::Cheat == opponent_action {
            self.was_cheated = true
        }
    }
}

#[derive(Default)]
pub struct CopycatAgent {
    opponent_action: Option<Action>,
}

impl Trait for CopycatAgent {
    fn play(&self) -> Action {
        match self.opponent_action {
            Some(action) => action,
            None => Action::Coop,
        }
    }

    fn react(&mut self, opponent_action: Action) {
        self.opponent_action = Some(opponent_action);
    }
}

#[derive(Default)]
pub struct DetectiveAgent {
    turn: usize,
    was_cheated: bool,
    opponent_action: Option<Action>,
}

impl Trait for DetectiveAgent {
    fn play(&self) -> Action {
        match self.turn {
            0 | 2 | 3 => Action::Coop,
            1 => Action::Cheat,
            _ => {
                if self.was_cheated {
                    self.opponent_action.unwrap()
                } else {
                    Action::Cheat
                }
            }
        }
    }

    fn react(&mut self, opponent_action: Action) {
        if Action::Cheat == opponent_action {
            self.was_cheated = true
        }
        self.opponent_action = Some(opponent_action);
        self.turn += 1;
    }
}
